import { Component, ViewChild } from '@angular/core';
import { NavController, Slides } from 'ionic-angular';
import { Facebook } from 'ionic-native';
import { Search } from '../search/search';
import { UserPhoto } from './userPhoto/userPhoto';

declare var AWS: any;

@Component({
  templateUrl: 'build/pages/login/login.html',
  directives: [UserPhoto]
})
export class Login {
  @ViewChild('mySlider') slider: Slides;

  test:any;
  mySlideOptions:any;
  photoUrl:String;
  user:Object;

  constructor(
    private navController: NavController
  ) {
    this.test = '';
    this.user = {};
    Facebook.getLoginStatus()
    .then( res => {
      if (res.status === 'connected') {
        this.test = JSON.stringify(res);
        //this.nav.setRoot(Page2);
      }
    });
    this.mySlideOptions = {
      onlyExternal: true
    };

  }

  fbLogin() {
    this.slider.update();
    this.photoUrl = 'google.com';
    Facebook.login(['email','public_profile'])
    .then( (res) => {
      this.test = JSON.stringify(res);
      this.slider.slideNext();

      AWS.config.region = 'us-east-1'; // Region
      AWS.config.credentials = new AWS.CognitoIdentityCredentials({
          IdentityPoolId: 'us-east-1:592b3530-bb98-455a-8ba4-a3249c5de284',
          Logins: {
            'graph.facebook.com': res.authResponse.accessToken
          }
      });

      Facebook.api('/v2.7/' + res.authResponse.userID + '/picture?redirect=0&type=normal&width=120&height120', [])
      .then((res) => {
        this.test = JSON.stringify(res);
        this.photoUrl = res.data.url;
      });

      Facebook.api('/v2.7/' + res.authResponse.userID, [])
      .then((res) => {
        this.user = JSON.stringify(res);
      });
    })
    .catch( (err) => {
      // Fb login failture
      this.slider.slidePrev();
    });
  }

  fbLogout() {
    Facebook.logout()
    .then((res) => {
      this.test = JSON.stringify(res);
      this.test += ' LOGGED OUT';
    })
  }
}
