import { Component, Input } from '@angular/core';
import {IONIC_DIRECTIVES} from 'ionic-angular';

@Component({
  selector: 'user-photo',
  templateUrl: 'build/pages/login/userPhoto/userPhoto.html',
  directives: [IONIC_DIRECTIVES]
})
export class UserPhoto {
  ele:any;
  constructor() {
  }
  @Input()
  set url(url:string) {
    this.ele = document.getElementById("userPhotoContainer");
    if (url) {
      this.ele.style.backgroundImage = "url('" + url +"')";
      this.ele.style.backgroundPosition = 'center';
      this.ele.style.backgroundRepeat = 'no-repeat';
      document.getElementById('userPhotoBlank').style.display = 'none';

    }
  }
}
